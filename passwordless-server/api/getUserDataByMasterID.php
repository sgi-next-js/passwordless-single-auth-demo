<?php
ob_start();
header('Content-Type: application/json; charset=utf-8');

$master_id = $_GET['masterid'];

$user = array();

if($master_id=='masterID_445fe77c6b2010d2b5e6296353373af250178708d66bc7dd57d3904181bff3dd'){
    $user["username"] = 'budi';
    $user["email"] = 'budi@mail.com';
    $user["role"] = 'ROLE_USER';
} elseif($master_id=='masterID_eb3809602ec71c9e19ead71509140d1fc61175f07c7b0dc54b7272ac567c7e84'){
    $user["username"] = 'badu';
    $user["email"] = 'badu@mail.com';
    $user["role"] = 'ROLE_ADMIN';  
} 

$result = array("status"=>"ok", "data"=>$user);

echo json_encode($result);
?>