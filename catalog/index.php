<?php
ob_start();
session_start();

echo 'Current masterID cookie: '.$_COOKIE['masterID'].'<br/><br/>';
if(isset($_SESSION['isLoggedIn'])){
    header('location: /main.php');
}
?>
<h3>Demo Passwordless Single Authentication</h3>
<p>Dalam demo ini ada 3 aplikasi: catalog, app1, app2. App1 dan App2 masing-masing memiliki halaman login sendiri dan mengelola user sendiri, sendiri. </p>
<p>Catalog adalah portal utama dari aplikasi-aplikasi tersebut. User bisa mengakses Catalog, lalu login, kemudian mengakses app1 dan app2 tanpa perlu login kembali</p>
<p> Apabila mau langsung akses ke masing-masing aplikasi, maka akan diminta untuk login ke masing2 aplikasi tersebut terlebih dahulu.<br/>
    Silahkan gunakan link dibawah ini:</p>
<ul>
    <li><a href="http://app1.127.0.0.1.nip.io:3000/" target="_blank">http://app1.127.0.0.1.nip.io:3000/ (NextJS)</a></li>
    <li><a href="http://app2.127.0.0.1.nip.io:8002/" target="_blank">http://app2.127.0.0.1.nip.io:8002/ (PHP)</a></li>
</ul>
<p> Untuk login melalui Catalog, silahkan login menggunakan <a href="/login.php">halaman ini</a>. Lalu akan muncul datalog aplikasi. Dan masing-masing aplikasi ini 
    akan mengenali siapa yang sedang login tanpa perlu masuk ke halaman login.</p>

