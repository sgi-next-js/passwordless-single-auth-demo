const secret = process.env.NEXTAUTH_SECRET;

function generateToken(basestring){
    const crypto = require('crypto');
    return crypto.createHash('sha256').update(basestring+secret).digest('hex');
}

export function loginWithPassword(email, password) {    
    let token = null;
    if((email=="budi@mail.com") &&
       (password=="next123")) {
        
        token = generateToken(email);
        console.log(token)
    }

    return token
}


export async function authWithMasterId(masterid) {
    let token = null;

    const result = await fetch('http://passwordless.127.0.0.1.nip.io:8003/api/getUserDataByMasterID.php?masterid='+masterid)
                            .then((res)=> res.json());
    console.log('auth.js result:', result);

    const user = result.data;
    console.log('auth.js user:', user);
    
    let email = user.email;
    console.log('auth.js user email:', email);

    let username = user.username;
    console.log('auth.js user name:', username);

    if((email=="budi@mail.com")) {
        
        token = generateToken(email);
        console.log('auth.js token:', token);
    }

    return [email, username, token];
}


export async function verifyAuthId(email, authid) {
    const token = generateToken(email);
    return token==authid
}