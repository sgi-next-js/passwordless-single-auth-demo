import * as cookie from 'cookie'
import { verifyAuthId } from '@/libs/auth';

export async function getServerSideProps(context) {
  const cookies = context.req.headers.cookie;
  
  try{
    const cookieStores = cookie.parse(cookies);
    var authid = cookieStores.authid;
    var email = cookieStores.email;
    
    console.log('mainjs authid', authid);
    console.log('mainjs email', email);

    if(!verifyAuthId(email, authid)){
      console.log("AuthID is invalid");

      return {
        redirect: {
          destination: '/',
          permanent: false,
        },
      }
    }
  } catch(e) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }

  return {
    props: {email},
  };
}


export default function Page({email}) {

  return (
    <main>
      <h3>Main Page</h3>
      <span> Welcome {email}</span>
      <br/>
      <br/>
      <a href="http://catalog.127.0.0.1.nip.io:8001/logout.php">Force Logout</a><br/><br/>
      <a href="http://catalog.127.0.0.1.nip.io:8001/main.php">Back to Catalog</a>
    </main>
  );
}