import LoginForm from '@/components/loginform';
import { authWithMasterId } from '@/libs/auth';
import {parse, serialize} from 'cookie'

export async function getServerSideProps(context) {
  const cookies = context.req.headers.cookie;
  
  console.log('index.js cookies: ', cookies);
  try {
    const cookieStores = parse(cookies);
    console.log('index.js cookieStores: ', cookieStores);
    var [email, username, token] = await authWithMasterId(cookieStores.masterID)

    context.res.setHeader('Set-Cookie', [serialize('authid', token, { path: '/' }),
                                   serialize('email', email, { path: '/' })]);

  } catch(e) {
    console.log(e);
  }

  if(token!=null) {
    console.log('index.js token: ', token);
    console.log('index.js username: ', username);
    console.log('index.js email: ', email);
    return {
      redirect: {
        destination: '/main',
        permanent: false,
      },
    }
  }

  return {
    props: {},
  };
}

export default function Index() {
    return (<>
      <LoginForm />
      <h3>Demo login App1 (Next.js)</h3>
      <br/>
      <p>Secara normal, user melakukan login ke aplikasi ini dengan email dan password. Tetapi jika cookie Master ID terdeteksi, 
        maka aplikasi ini akan melakukan validasi ke Passwordless Server, lalu mendapatkan email user, dan secara otomatis akan 
        melakukan autentikasi tanpa form login.</p>
      <p>User yang terdaftar pada aplikasi ini adalah:<br/>
      email: <strong>budi@mail.com</strong><br/>
      password: <strong>next123</strong><br/>
      </p>  
    </>); 
}
