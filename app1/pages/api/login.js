import { loginWithPassword } from "@/libs/auth";
import { serialize } from 'cookie';


export default function loginhandler(req, res) {
  if (req.method !== 'POST') {
    res.status(405).send({ message: 'Only POST requests allowed' })
    return
  }
  const body = req.body
  const token = loginWithPassword(body.email, body.password);
  if(token!=null){
      res.setHeader('Set-Cookie', [serialize('authid', token, { path: '/' }),
                                   serialize('email', body.email, { path: '/' })]);
      res.redirect('/main',301);
  } else {
    res.redirect('/', 401)
  }  
}
