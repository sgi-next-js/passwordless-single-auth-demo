import { loginWithPassword } from "@/libs/auth";
import { serialize } from 'cookie';


export default function logouthandler(req, res) {
  res.setHeader('Set-Cookie', [serialize('authid', '', { path: '/' }),
                               serialize('email', '', { path: '/' })]);
  res.redirect('/', 401)  
}